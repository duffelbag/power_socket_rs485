/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define AC_V_Pin GPIO_PIN_5
#define AC_V_GPIO_Port GPIOA
#define AC_I_DIFF_Pin GPIO_PIN_6
#define AC_I_DIFF_GPIO_Port GPIOA
#define AC_I_Pin GPIO_PIN_7
#define AC_I_GPIO_Port GPIOA
#define TH1_Pin GPIO_PIN_0
#define TH1_GPIO_Port GPIOB
#define TH2_Pin GPIO_PIN_1
#define TH2_GPIO_Port GPIOB
#define RELAY_Pin GPIO_PIN_12
#define RELAY_GPIO_Port GPIOB
#define RS_ACT_LED_Pin GPIO_PIN_13
#define RS_ACT_LED_GPIO_Port GPIOB
#define USART_TX_Pin GPIO_PIN_9
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_10
#define USART_RX_GPIO_Port GPIOA
#define USART_DE_Pin GPIO_PIN_12
#define USART_DE_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/*
 * Настройки аналоговых приборов
 */
#define VDD_AD	3.55	   ///< Напряжение питания цифровой и аналоговой части
#define ADC_SLOPE VDD_AD/4096//0.000806

#define TH1_B	3950	   ///< Параметр 'B' первого термодатчика
#define TH1_R1	10000	   ///< Резистор в делителе первого термодатчика
#define TH1_T0	298.16	   ///< Температура T0 первого термодатчика
#define TH2_B	3950	   ///< Параметр 'B' второго термодатчика
#define TH2_R1	10000	   ///< Резистор в делителе второго термодатчика
#define TH2_T0	298.16	   ///< Температура T0 второго термодатчика

#define CPU_T_V25		1.41	///< Напряжение (в вольтах) на внутреннем датчике при температуре 25 °C.
#define CPU_T_VSLOPE	0.0043	///< Изменение напряжения (в вольтах) при изменении температуры на градус.

#define VOLTAGE_OFFSET	2046	///< Ноль для измеренного напряжения в значении АЦП
#define V_R2_R1 182000/680		///< Отношение резисторов в трансформаторе напряжения
#define CURRENT_OFFSET	2046	///< Ноль для измеренного тока в значении АЦП
#define I_R 75					///< Резистор токового трансформатора

#define FIRMWARE_VERSION 1	///< версия микропрограммы
#define BUFSIZE	1024		///< размер буфера приема RS485
#define MBADDR	11			///< MODBUS адрес устройства
#define MBINPREGS	14		///< количество MODBUS INPUT регистров (только для чтения)
#define MBHOLDREGS	10		///< количество MODBUS HOLDING регистров (чтение и запись)
#define VARRSIZE	200		///< размер массива значений напряжения и тока. 200 это 1/4 сек
/*
 * Modbus input registers
 */
#define MOD_CURRENT_RMS 0	///< MODBUS input reg: cреднеквадратичный ток
#define MOD_VOLTAGE_RMS 1	///< MODBUS input reg: cреднеквадратичное напряжение
#define MOD_CURRENT_PEAK 2	///< MODBUS input reg: пиковый ток
#define MOD_VOLTAGE_PEAK 3	///< MODBUS input reg: пиковое напряжение
#define MOD_WATT 4			///< MODBUS input reg: потребляемая мощность
#define MOD_WATT_HOURS 5	///< MODBUS input reg: ватт-часы накопительный
#define MOD_TH1 6			///< MODBUS input reg: температура TH1
#define MOD_TH2 7			///< MODBUS input reg: температура TH2
#define MOD_THERMO 8		///< MODBUS input reg: температура микроконтроллера
#define MOD_FIRMWARE_VER 9	///< MODBUS input reg: версия микропрограммы
#define MOD_ERROR_COUNT 10	///< MODBUS input reg: счетчик ошибок
#define MOD_LAST_ERROR 11	///< MODBUS input reg: номер последней ошибки
#define MOD_I_AVG 12		///< MODBUS input reg: среднее значение АЦП тока за период измерения
#define MOD_V_AVG 13		///< MODBUS input reg: среднее значение АЦП напряжения за период измерения
/*
 * Modbus holding registers
 */
#define MOD_RELAY 0			///< MODBUS holding reg: реле 0 - выкл, 1 - вкл.
#define MOD_PEAK_RESET 1	///< MODBUS holding reg: сброс пикового значения напряжения и тока
#define MOD_ERRCNT_RESET 2	///< MODBUS holding reg: сброс счетчика ошибок
/*
 * Internal errors
 */
#define UART_ERROR_PE 1 	///< USART Parity error
#define UART_ERROR_NE 2 	///< USART Noise error
#define UART_ERROR_FE 3 	///< USART Frame error
#define UART_ERROR_ORE 4 	///< USART Overrun error
#define UART_ERROR_DMA 5 	///< USART DMA transfer error
#define UART_ERROR_BUFFER 6 	///< USART buffer owerflow
#define UART_ERROR_HAL 7 	///< USART HAL_ERROR - rx_buff == NULL or rx_buff_len == 0
#define MOD_ERROR_CRC 8	///< MODBUS crc error

void Modbus_error(int err_num);
void Internal_error(uint16_t err_num);
uint8_t Modbus_do(uint16_t addr, uint16_t data);
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
